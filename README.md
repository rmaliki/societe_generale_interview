# Société Générale interview

### How to run the notebook:
1. Open a bash or gitbash terminal in the project directory.
2. Create a conda python 3.7 environment :
conda create --prefix ./venv  python=3.7 --no-default-packages --yes || true
3. Activate the newly created environment:
source activate ./venv
4. Install requirements.txt :
pip install -r requirements.txt
5. Add env to ipython kernel:
ipython kernel install --user --name=interview_env
6. Launch :
jupyter notebook
7. Change the kernel to interview_env
